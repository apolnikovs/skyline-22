{block name=scripts}
    <script>
        $(document).ready(function() {
            $( ".gaSortable" ).sortable({
                update:function(event,ui){ 
                    var hiddeninput = $(this).parent().find('input[type=hidden]'); 
                    hiddeninput.val(hiddeninput.val()==0?1:0);
                }
            });

            $('#gauge_chart_div').tabs({ 
                cache:false,
                selected:{if $cjBy === 'c'}1{else}0{/if},
                activate: function( event, ui ) { 
                    $(ui.newTab).css({ 
                        'background': '#25AAE1',
                        'color': '#FFF'
                    });
                    $(ui.oldTab).css({ 
                        'background': '#FFF',
                        'color': '#737373'
                    });

                    var cjBy = 'g';
                    if($(ui.newTab).attr('id') == "charttab"){ 
                        drawChart();
                        cjBy = 'c';

                    }
                    else{ 
                        cjBy ='g';
                        {foreach from=$gauge key=ini item=individual_gauge}
                            gauge{$ini}.draw({$individual_gauge.value}, gauge{$ini}_options);
                        {/foreach}
                    }

                    $.ajax({
                        url:'{$_subdomain}/Job/setchart/',
                        data:'cjBy='+cjBy+"&ojvBy="+$(".ojvByElement:checked").val(),
                        type:'post',
                        dataType:'html',
                        success:function(){
                        }
                    });
                }
            });
            
            jQuery.validator.addMethod('requiredMultiple',function(value,element,param){ 
                var notEqual = true;
                $(param).each(function(){ 
                    value = $.trim($(this).val());
                    if(value == ""){ 
                        notEqual = false;
                        $(this).addClass('fieldError');
                    }
                });
                return notEqual;
            },"{$page['Errors']['select_closed']|escape:'html'}");
        
        
            jQuery.validator.addMethod('digitsMultiple',function(value,element,param){ 
                 var notEqual = true;
                 $(param).each(function(){
                     value = $.trim($(this).val());
                     if(!/^\d+$/.test(value)){ 
                         notEqual = false;
                         $(this).addClass('fieldError');
                     }
                 });
                 return notEqual;
             },"{$page['Errors']['select_closed']|escape:'html'}");
        
            jQuery.validator.addMethod('maxMultiple',function(value,element,param){ 
                 var notEqual = true;
                 $(param).each(function(){
                     value = $.trim($(this).val());
                     if(value > 100){ 
                         notEqual = false;
                         $(this).addClass('fieldError');
                     }
                 });
                 return notEqual;
             },"{$page['Errors']['select_closed']|escape:'html'}");
        
            jQuery.validator.addMethod("notEqualTo",
                function(value, element, param) {
                    var notEqual = true;
                    value = $.trim(value); 
                    var i = 0; 

                    $(param).each(function(){
                        if(value === $.trim($(this).val())){ 
                            i++;
                            $(this).addClass('fieldError');
                        }
                    });
                    if(i > 1){ 
                        notEqual = false; 
                    }

                    return notEqual;
                },
                "{$page['Errors']['not_equal']|escape:'html'}"
            );
            
            jQuery.validator.addMethod("greaterTo",
                function(value, element,param) { 

                    value = $.trim(value);
                    var notEqual = true;
                    var i = 0; 

                    $(param).each(function(){ 

                        parentNodeElement = $(this).parents().eq(3); 

                        var hiddeninput    = parentNodeElement.find('input[type=hidden]');
                        var inputfldYellow = parentNodeElement.find('input.GaDialYellow');
                        var inputfldRed    = parentNodeElement.find('input.GaDialRed');

                        if(hiddeninput.val() == 1){
                            if($.trim(inputfldRed.val()) > $.trim(inputfldYellow.val())){ 
                                notEqual = false; 
                                inputfldRed.addClass('fieldError');
                                inputfldYellow.addClass('fieldError');
                            }
                        }
                        else{ 
                            if($.trim(inputfldYellow.val()) > $.trim(inputfldRed.val())){ 
                                notEqual = false; 
                                inputfldRed.addClass('fieldError');
                                inputfldYellow.addClass('fieldError');
                            }
                        }
                    });

                    return  notEqual;
                },
                "{$page['Errors']['greater_value']|escape:'html'}"
            );
            
            jQuery.validator.addMethod("lessTo",
                function(value, element,param) {
                    value = $.trim(value);
                    var notEqual = true;

                    for (i = 0; i < param.length; i++) { 
                        if (value >= $.trim($(param[i]).val())) { 
                            notEqual =false;    
                        }
                    }

                    return  notEqual;
                },
                "{$page['Errors']['sum_not_equal']|escape:'html'}"
            );
            
            $(document).on('click', "#gauge_insert_save_btn", function(event) { 
                if($("label.fieldError:visible").length>0) {
                }
                else
                {
                    $("#gauge_insert_save_btn").hide();
                    $("#gauge_cancel_btn").hide();
                    $("#gauge_processDisplayText").show();
                }
            
                $("#GaugePreferencesForm").validate({
                    ignore: '',
                    rules: {
                        'GaDialStatusID[]':{
                            requiredMultiple: 'Select.GaDialStatusSelect',
                            notEqualTo:'Select.GaDialStatusSelect'
                        },
                        'GaDialYellow[]':{ 
                            requiredMultiple: 'input.GaDialYellow',
                            digitsMultiple:'input.GaDialYellow',
                            maxMultiple: 'input.GaDialYellow',
                            greaterTo:'input[type=number]',
                        },
                        'GaDialRed[]':{ 
                            requiredMultiple: 'input.GaDialRed',
                            digitsMultiple:'input.GaDialRed',
                            maxMultiple: 'input.GaDialRed',
                            greaterTo:'input[type=number]'
                        },
                    },
                    messages: { 
                        'GaDialStatusID[]': {
                            requiredMultiple: "{$page['Errors']['select_closed']|escape:'html'}",
                            notEqualTo:"{$page['Errors']['not_equal']|escape:'html'}"
                        },
                        'GaDialYellow[]':{ 
                            requiredMultiple: '{$page['Errors']['text_percent']|escape:'html'}',
                            digitsMultiple:'{$page['Errors']['text_percent']|escape:'html'}',
                            maxMultiple: '{$page['Errors']['text_percent']|escape:'html'}',
                            greaterTo: '{$page['Errors']['greater_value']|escape:'html'}',
                        },
                        'GaDialRed[]':{ 
                            requiredMultiple: '{$page['Errors']['text_percent']|escape:'html'}',
                            digitsMultiple:'{$page['Errors']['text_percent']|escape:'html'}',
                            maxMultiple: '{$page['Errors']['text_percent']|escape:'html'}',
                            greaterTo: '{$page['Errors']['greater_value']|escape:'html'}',
                        },
                    },
                    errorPlacement: function(error) { 
                        $(".GaDialError").append(error);
                        $("#gauge_insert_save_btn").show();
                        $("#gauge_cancel_btn").show();
                        $("#gauge_processDisplayText").hide();
                    },
                    errorClass: 'fieldError',
                    errorElement: 'label',
                    onkeyup: false,
                    select: false,
                    onclick: false,
                    onblur: false,
                    submitHandler: function() { 
                        $("#gauge_insert_save_btn").hide();
                        $("#gauge_cancel_btn").hide();
                        $("#gauge_processDisplayText").hide();
        
                        $.post("{$_subdomain}/Data/updateUserGaugePreferences",        
                            $("#GaugePreferencesForm").serialize(),      
                            function(data){
                                //var p = eval("(" + data + ")");
                                document.location.href = "{$_subdomain}/Job/closedJobs"
                        }); //Post ends here...
                    }
                });
            });
            
            
            
            // set up gauge options 
            {foreach from=$gauge key=ini item=individual_gauge} 
                // Instantiate our gauge object.
                var gauge{$ini} = new pccs.Gauge('gauge{$ini}');
        
                {if $individual_gauge.Swap == 1}
                    var gauge{$ini}_options = { 
                        gauge_colour : '#23A274',
                        red_colour : '#ff0000',
                        scale_start: {$individual_gauge.min},
                        scale_inc: {$individual_gauge.inc},
                        scale_divisions: {$individual_gauge.div},
                        red_start: {$individual_gauge.yellow},
                        red_end: {$individual_gauge.inc*$individual_gauge.div},
                        amber_start: {$individual_gauge.red},
                        amber_end: {$individual_gauge.yellow},
                        show_minor_ticks: false 
                    };
                {else}
                    var gauge{$ini}_options = { 
                        gauge_colour : '#FF0000',
                        red_colour : '#23A274',
                        scale_start: {$individual_gauge.min},
                        scale_inc: {$individual_gauge.inc},
                        scale_divisions: {$individual_gauge.div},
                        red_start: {$individual_gauge.red},
                        red_end: {$individual_gauge.inc*$individual_gauge.div},
                        amber_start: {$individual_gauge.yellow},
                        amber_end: {$individual_gauge.red},
                        show_minor_ticks: false 
                    };
                {/if}
                // Draw our gauge.
                gauge{$ini}.draw({$individual_gauge.value}, gauge{$ini}_options );
            {/foreach}
                
            
            gauge_row = function(){
                var gaugeRow = '<tr><td><label class="fieldLabel">&nbsp;</label><select name="GaDialStatusID[]" class="GaDialStatusSelect select reuired">';
                {foreach from=$completionStatus key=comkey item=comStatus}
                   gaugeRow += '<option value="{$comkey}" >{$comStatus}</option>';
                {/foreach}
                gaugeRow += '</select></td><td><input type="hidden" name="GaDialSwap[]" value="0" >';
                gaugeRow += '<ul class="gaSortable"><li><span style="display: inline-block;padding:5px 10px;font-weight: normal;width:50px;background: #F00;color: #000;text-align: center;">{$page['Labels']['red_text']|escape:'html'}</span> < <span style="display: inline-block;"><input type="number" style="width: 60px;height: 20px;margin-left: 10px;" name="GaDialYellow[]" class="GaDialYellow reuired" min="0" max="100" value="0"></span></li>';
                gaugeRow += '<li><span style="display: inline-block;padding:5px 10px;font-weight: normal;width:50px;background: #0F0;color: #000;text-align: center;">{$page['Labels']['green_text']|escape:'html'}</span> > <span style="display: inline-block;"><input type="number" style="width: 60px;height: 20px;margin-left: 10px;" name="GaDialRed[]" class="GaDialRed reuired" min="0" max="100" value="0"></span></li></ul>';
                gaugeRow += '<div style="clear: both;"></div></td><td><img src="{$_subdomain}/images/delete.png" class="gauge_preferencesdeleteHelp" style="margin-right:20px;cursor: pointer;" title="{$page['Text']['add_info']|escape:'html'}" alt="{$page['Text']['add_info']|escape:'html'}" ></td>';
                gaugeRow += '</tr>';
                return gaugeRow;
            };
                
            var rowvalue = gauge_row();
        
            $(document).on("click", "#gauge_preferencesadd", function() { 
               $(rowvalue).insertBefore($('#allotherrow'));
            });
            
            $(document).on("click", ".gauge_preferencesdeleteHelp", function() { 
                var rownum = $('#gauge_preferences_table tr').length -3;
                if(rownum == 1){
                    var cnf = confirm("This is a last gauge Setting, Are you sure to delete it.");
                    if(cnf){ 
                      $(this).parent().parent().remove();
                    }
                }
                else{ 
                    $(this).parent().parent().remove();
                }
            });
            
            $(document).on('click', "#GaugePreferences", function() { 
                //It opens color box popup page.              
                $.colorbox( {   inline:true,
                    href:"#DivGaugePreferences",
                    title: '',
                    opacity: 0.75,
                    height:750,
                    width:900,
                    overlayClose: false,
                    escKey: false,
                    onLoad: function() {
                       // $('#cboxClose').remove();
                    },
                    onClosed: function() {

                       //location.href = "#EQ7";
                    },
                    onComplete: function()
                    {
                        $.colorbox.resize();
                    }
                 }); 
            });
        });
    </script>
{/block}