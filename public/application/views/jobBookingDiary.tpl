{extends "DemoLayout.tpl"}

{block name=config}
{$Title = $page['Text']['page_title']|escape:'html'}
{$PageId = $JobBookingDiaryPage}
{/block}

{block name=scripts}
<style >
   body{ overflow-x:hidden;overflow-y:hidden; }
</style>
 
 
<script type="text/javascript">
      
      $(document).ready(function() {
      
      
      /* =======================================================
                *
                * Initialise input auto-hint functions...
                *
                * ======================================================= */

                $('.auto-hint').focus(function() {
                        $this = $(this);
                        if ($this.val() == $this.attr('title')) {
                            $this.val('').removeClass('auto-hint');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','password');
                            }
                        }
                        else if ($this.val()!='' && $this.val() != $this.attr('title')) {
                             $this.removeClass('auto-hint'); 
                        }
                        
                        
                    } ).blur(function() {
                        $this = $(this);
                        if ($this.val() == '' && $this.attr('title') != '')  {
                            $this.val($this.attr('title')).addClass('auto-hint');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','text');
                            }
                        } else if ($this.val()!='' && $this.val() != $this.attr('title')) {
                             $this.removeClass('auto-hint'); 
                        }
                        
                        
                    } ).each(function(){
                        $this = $(this);
                        
                        if ($this.attr('title') == '') { return; }
                        if ($this.val() == '') { 
                            if ($this.attr('type') == 'password') {
                                $this.addClass('auto-pwd').prop('type','text');
                            }
                            $this.val($this.attr('title')); 
                        } else {
                        
                                $this.removeClass('auto-hint'); 
                        }
                        $this.attr('autocomplete','off');
                    } );
       
       
       
               /* =======================================================
                *
                * set tab on return for input elements with form submit on auto-submit class...
                *
                * ======================================================= */

                $('input[type=text],input[type=password]').keypress( function( e ) {
                        if (e.which == 13) {
                            $(this).blur();
                            if ($(this).hasClass('auto-submit')) {
                                $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                                } );
                                $(this).get(0).form.onsubmit();
                            } else {
                                $next = $(this).attr('tabIndex') + 1;
                                $('[tabIndex="'+$next+'"]').focus();
                            }
                            return false;
                        }
                    } );   
                    
                    
                    
                    
          $(document).on('click', '#update_save_btn', 
                        function() {
                        
                            //Validating and posting form data.
                                      
                            $('.auto-hint').each(function() {
                                         $this = $(this);
                                         if ($this.val() == $this.attr('title')) {
                                             $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                             if ($this.hasClass('auto-pwd')) {
                                                 $this.prop('type','password');
                                             }
                                         }
                             } );
                        
                            $("#processDisplayText").show();
                            $("#update_save_btn").hide();
                            $("#cancel_btn").hide();
                        
                        
                        
                             $('#EditAddressForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                               
                                                ColAddPostcode:
                                                    {
                                                        required: true
                                                    },
                                               
                                               ColAddStreet:
                                                    {
                                                        required: true
                                                    },
                                              
                                               ColAddCity:
                                                    {
                                                        required: true
                                                    },
                                              
                                               ColAddCountryID:
                                                    {
                                                       required: true
                                                    },
                                                ContactEmail:
                                                {
                                                     
                                                    email: true
                                                },
                                                ContactHomePhone:
                                                    {
                                                       // required: true,
                                                        digits: true
                                                    },
                                                ContactMobile:
                                                    {
                                                        digits: true
                                                    }
                                                    
                                                    
                                                    
                                                    
                                               },
                                            messages: {
                                                        
                                                        ColAddPostcode:
                                                        {
                                                            required: "{$page['Errors']['postal_code']|escape:'html'}"

                                                        },
                                                        ColAddStreet:
                                                        {
                                                            required: "{$page['Errors']['street']|escape:'html'}"

                                                        },
                                                        ColAddCity:
                                                        {
                                                            required: "{$page['Errors']['city']|escape:'html'}"

                                                        },
                                                        ColAddCountryID:
                                                        {
                                                            required: "{$page['Errors']['country']|escape:'html'}"

                                                        },
                                                        ContactEmail:
                                                        {
                                                            
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"

                                                        },
                                                        ContactHomePhone:
                                                        {
                                                          
                                                            digits: "{$page['Errors']['valid_daytime_phone']|escape:'html'}"

                                                        },
                                                        ContactMobile:
                                                        {
                                                            digits: "{$page['Errors']['valid_mobile_no']|escape:'html'}"
                                                        }
                                                        
                                                        
                                             }, 
                                            errorPlacement: function(error, element) {
                                                
                                               
                                                    error.insertAfter( element );
                                                    
                                                    $("#processDisplayText").hide();
                                                    $("#update_save_btn").show();
                                                    $("#cancel_btn").show();
                                                          
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                    
                                                    
                                                    
                                                    $.post("{$_subdomain}/Job/updateColAddress/",         
                                                    $("#EditAddressForm").serialize(),      
                                                               function(data){

                                                               var p = eval("(" + data + ")");

                                                               if(p=="OK")
                                                               {
                                                                   
                                                                   var $AppointmentAddressElement = '';
                                                                 
                                                                 
                                                                   $AppointmentAddressElement = $("#CustomerFullName").val();
                                                                   
                                                                   
                                                                    $('.addressGroup').each(function() {
                                                                    
                                                                        if($(this).val()) 
                                                                        {
                                                                              if($AppointmentAddressElement!='')
                                                                              {
                                                                                   $AppointmentAddressElement += ",<br>";   
                                                                              }

                                                                              $AppointmentAddressElement += $(this).val(); 
                                                                        }
                                                                    
                                                                     });
                                                                   
                                                                  
                                                                   
                                                                   
                                                                 $("#AppointmentAddressElement").html($AppointmentAddressElement);  
                                                                 
                                                                  $("#processDisplayText").hide();
                                                                  $("#update_save_btn").show();
                                                                  $("#cancel_btn").show();
                                                                 
                                                                  $.colorbox.close();
                                                                 
                                                                  // $(location).attr('href', '{$_subdomain}/Job/raJobs'+atTypeParam+"/sType="+$("#NewRAStatus").val());         
                                                               }
                                                               else 
                                                               {
                                                                   $("#suggestText").html("{$page['Errors']['save_changes_error']|escape:'html'}").css('color','red').fadeIn('slow');
                                                                   
                                                                   
                                                                   $("#processDisplayText").hide();
                                                                   $("#update_save_btn").show();
                                                                   $("#cancel_btn").show();
                                                                   
                                                               }

                                                                


                                                   });
                                                    
                                                   
                                                                 
                                                     
                                                }
                                            });
                        
                        
                        });
      
      
      
     
        $(document).on('click', '#cancel_btn', 
            function() {
                
                    $.colorbox.close();
            
            });
        
        
         $(document).on('click', '#EditAppointmentAddress', 
            function() {
            
            
                   //It opens color box popup page.              
                    $.colorbox( {   inline:true,
                                    href:"#DivEditAddress",
                                    title: '',
                                    opacity: 0.75,
                                    height:540,
                                    width:720,
                                    overlayClose: false,
                                    escKey: false,
                                    onLoad: function() {
                                       // $('#cboxClose').remove();
                                    },
                                    onClosed: function() {

                                       //location.href = "#EQ7";
                                    },
                                    onComplete: function()
                                    {
                                        $.colorbox.resize();
                                    }

                                 }); 
                
                  return false;
            
            });
        
        
        
         $(document).on('click', '#confirm_appointment_btn', 
            function() {

           var $appointment_booked = "{$appointment_booked}";
               
           $("input[name=appointment_slot]:radio").each(function() {
       
                if($(this).is(":checked")) 
                {
                    if($(this).val()=="ANY")
                    {
                            $appointment_booked = $appointment_booked+"<BR>Anytime Slot ";
                    }
                    else if($(this).val()=="AM")
                    {
                            $appointment_booked = $appointment_booked+"<BR>AM Slot";
                    }
                    else if($(this).val()=="PM")
                    {
                            $appointment_booked = $appointment_booked+"<BR>PM Slot";
                    }
                }   
                
            });
          
            
           window.opener.$('#BookAppointmentText').html($appointment_booked+"<br><br><a href='#' id='BookAppointmentLink'  >Change</a>");
           
            window.close();
                  

            }      
        );
        
        
       
        
      
      });
      
</script>

{/block}

{block name=body}


        
<div class="main" id="home">
       
   <form id="jobBookingDiaryForm" name="jobBookingDiaryForm" method="post"  action="#" class="inline">         
    
    <div class="jobBookingDiaryPanel" style="height:500px;float:left;"  >
        
           
        
         
       
             
            <fieldset>
            <legend>{$page['Text']['legend']|escape:'html'}</legend>
             
            <p>
               <img  src="{$_subdomain}/css/Skins/{$_theme}/images/appointment_diary_dummy.png" width="550" height="430"  />
            </p>
           
         </fieldset> 

        
        
          
    </div>   
                                
    <div class="jobBookingDiaryRightPanel" style="height:500px;float:left"  >
        
        
        
            <fieldset>
               <legend style="font-size:12px;" >{$page['Text']['confirm_appointment_address']|escape:'html'}:</legend>
               
               <p>
               
                    <span id="AppointmentAddressElement" style="padding:0px;margin:0px;" >{$appointmentAddress}</span>
                    
                    <span style="float:right;padding:0px;margin:0px;" ><a href="#" id="EditAppointmentAddress" >{$page['Buttons']['edit']|escape:'html'}</a></span>
                   
               </p>
               
                   
            </fieldset>
               
             <fieldset>
               <legend style="font-size:12px;" >{$page['Text']['select_appointment_slot']|escape:'html'}:</legend>
               <p>
                   <img id="appointment_slot_tooltip" title="{$page['Text']['select_appointment_slot_tooltip']|escape:'html'}"  alt="{$page['Text']['select_appointment_slot_tooltip']|escape:'html'}" style="float:right;cursor:pointer;"  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" width="16" height="16"  />
               </p>
               <p>
               
                  {$page['Text']['select_appointment_slot_text']|escape:'html'}
                   
               </p>
               <p>
                   <input type="radio" name="appointment_slot" value="ANY" checked="checked" ><span class="slotText" >ALL DAY</span>
                   <input type="radio" name="appointment_slot" value="AM" ><span class="slotText" >AM ONLY</span>
                   <input type="radio" name="appointment_slot" value="PM" ><span class="slotText" >PM ONLY</span>
               </p>
            </fieldset>   
               
               
             <fieldset>
               <legend style="font-size:12px;" >{$page['Text']['record_appointment_notes']|escape:'html'}:</legend>
               
               <p>
               
                   <textarea style="width:210px;height:90px;" maxlength="500" ></textarea>   

                   
               </p>
            </fieldset>   
           
            <p>&nbsp;</p>   
               
            <p style="text-align:center;" >
           
            <input type="submit" name="confirm_appointment_btn" id="confirm_appointment_btn" class="textSubmitButton"  value="{$page['Buttons']['confirm_appointment']|escape:'html'}" >
           
            </p>

    </div>
    
   </form>    
               
</div>
            
            
<div style="display:none;" >

        
         <div id="DivEditAddress" class="SystemAdminFormPanel" >
    
                <form id="EditAddressForm" name="EditAddressForm" method="post"  action="#" class="inline" >

                <fieldset>
                    <legend title="" >{$page['Text']['edit_address']|escape:'html'}</legend>

                    <p style="text-align:center;" ><label id="suggestText" style="width:400px;" ></label></p>
                           

                    
                    <p  id="jbColAddCompanyNameElement" >
                    <label for="jbColAddCompanyName">{$page['Labels']['business_name']|escape:'html'}:</label>
                     &nbsp;&nbsp; <input type="text" class="text capitalizeText addressGroup" name="ColAddCompanyName" value="{$jb_datarow.ColAddCompanyName|escape:'html'}"  id="jbColAddCompanyName" >

                    </p> 


                    <p id="jb_p_c_a_house_name"  >
                        <label for="jbColAddBuildingName" >{$page['Labels']['building_name']|escape:'html'}:</label>
                        &nbsp;&nbsp; <input type="text" class="text capitalizeText addressGroup"  name="ColAddBuildingNameNumber" value="{$jb_datarow.ColAddBuildingName|escape:'html'}" id="jbColAddBuildingName" >          
                    </p>

                    <p id="jb_p_c_a_street"  >
                        <label for="jbColAddStreet" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                        &nbsp;&nbsp; <input type="text" class="text capitalizeText addressGroup"  name="ColAddStreet" value="{$jb_datarow.ColAddStreet|escape:'html'}" id="jbColAddStreet" >          
                    </p>

                    <p id="jb_p_c_a_area"  >
                        <label for="jbColAddArea" >{$page['Labels']['area']|escape:'html'}:</label>
                        &nbsp;&nbsp; <input type="text" class="text capitalizeText addressGroup" name="ColAddLocalArea" value="{$jb_datarow.ColAddArea|escape:'html'}" id="jbColAddArea" >          
                    </p>


                    <p id="jb_p_c_a_town"  >
                        <label for="jbColAddCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>
                        &nbsp;&nbsp; <input type="text" class="text capitalizeText addressGroup" name="ColAddCity" value="{$jb_datarow.ColAddCity|escape:'html'}" id="jbColAddCity" >          
                    </p>

                    <p id="jb_p_c_a_county"  >
                        <label for="jbColAddCounty" >{$page['Labels']['county_geographic_area']|escape:'html'}:</label>
                        &nbsp;&nbsp;  


                        <select name="ColAddCountyID" id="jbColAddCounty" class="text" >
                            <option value="" id="cacountry_0_county_0" {if $jb_datarow.ColAddCounty eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                            {foreach $countries as $country}


                                {foreach $country.Counties as $county}
                                <option value="{$county.CountyID}"  id="cacountry_{$country.CountryID}_county_{$county.CountyID}" {if $jb_datarow.ColAddCounty eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                                {/foreach}


                            {/foreach}

                        </select>

                    </p>

                    <p id="jb_p_c_a_country" >
                        <label for="jbColAddCountry" >{$page['Labels']['country']|escape:'html'}: <sup>*</sup></label>
                        &nbsp;&nbsp; 
                            <select name="ColAddCountryID" id="jbColAddCountry" >
                            <option value="" {if $jb_datarow.ColAddCountry eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                            {foreach $countries as $country}

                                <option value="{$country.CountryID}" {if $jb_datarow.ColAddCountry eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                            {/foreach}

                        </select>
                    </p>
                    
                    
                    <p id="jbColAddPostalCodeElement" >
                            <label for="jbColAddPostalCode" id="jbColAddPostalCodeLabel" >
                                {$page['Labels']['collection_postcode']|escape:'html'}:<sup>*</sup>
                            </label>
                            &nbsp;&nbsp; 
                            <input class="text uppercaseText addressGroup" style="width:230px;" type="text" name="ColAddPostcode" id="jbColAddPostalCode" value="{$jb_datarow.ColAddPostalCode|escape:'html'}" />
                           
                      </p>
                    
                    
                     <p>
                        <label for="jbContactEmail" id="jbContactEmailLabel" >{$page['Labels']['email']|escape:'html'}:<sup id="jbContactEmailSup" >*</sup></label>
                          &nbsp;&nbsp; <input  type="text" name="ContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$jb_datarow.ContactEmail|escape:'html'}" id="jbContactEmail" >
                        


                     </p>

                     <p>
                         <label for="jbContactHomePhone" id="jbContactHomePhoneLabel" >{$page['Labels']['daytime_phone']|escape:'html'}:</label>
                         &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ContactHomePhone" value="{$jb_datarow.ContactHomePhone|escape:'html'}" id="jbContactHomePhone" >
                         <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ContactHomePhoneExt" value="{$jb_datarow.ContactHomePhoneExt|escape:'html'}" id="ContactHomePhoneExt" >

                     </p>

                     <p>
                         <label for="jbContactMobile" id="jbContactMobileLabel" >{$page['Labels']['mobile_no']|escape:'html'}:</label>
                         &nbsp;&nbsp; <input  type="text" class="text auto-hint" name="ContactMobile" class="text auto-hint" id="jbContactMobile" title="{$page['Text']['mobile']|escape:'html'}"   value="{$jb_datarow.ContactMobile|escape:'html'}" >

                     </p>
                    

                    <p>

                         <label>&nbsp;</label>
                         &nbsp;&nbsp;
                         <input type="submit" name="update_save_btn" class="textSubmitButton" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                           &nbsp;

                                <input type="submit" name="cancel_btn" class="textSubmitButton" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >

                                <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Text']['process_record']|escape:'html'}</span>    

                        
                                <input type="hidden" name="JobID" id="JobID"  value="{$JobID|escape:'html'}" >
                                <input type="hidden" name="CustomerID" id="CustomerID"  value="{$CustomerID|escape:'html'}" >
                                
                                <input type="hidden" name="CustomerFullName" id="CustomerFullName"  value="{$CustomerFullName|escape:'html'}" >
                                
                                

                    </p>







                </fieldset>    

                </form>        


        </div>
        
        

    </div>            
               
           
{/block}