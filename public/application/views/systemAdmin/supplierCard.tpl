
<script type="text/javascript">
    $(function() {
        $( "#tabs" ).tabs();
    });
    $(document).ready(function() {
        $("#CountryID").combobox();
        $("#DirectOrderTemplateID").combobox();
        $("#DefaultCurrencyID").combobox();
    });
</script>   

<div id="SuppliersFormPanel" class="SystemAdminFormPanel" >
     <fieldset>
            <legend>Insert Supplier</legend>
   <form id="SuppliersForm" name="SuppliersForm" method="post"  action="{$_subdomain}/OrganisationSetup/saveSupplier" class="inline">
       
       <input type="hidden" name="mode" value="{$mode}">
       
       {if isset($data)}
       <input type="hidden" name="id" value="{if !isset($data['ServiceProviderSupplierID'])}{$data['SupplierID']}{else}{$data['ServiceProviderSupplierID']}{/if}">
      
       {/if}
        <input type="hidden" name="ServiceProviderID" value="{$ServiceProviderID}">
           <input type="hidden" name="CreatedDate" value="{$data.CreatedDate|default:''}">
       
       <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Company Details</a></li>
    <li><a href="#tabs-2">Part Order Settings</a></li>
    
  </ul>
  <div id="tabs-1" class="SystemAdminFormPanel inline">
  
       
         
            <p><label id="suggestText" ></label></p>
                            
                            <p>
                                <label ></label>
                                <span class="topText cardTopLabel" style="font-size:12px!important;color:#737373">Fields marked with a <sup>*</sup> are compulsory</span>
                            </p>
                       
                            
                           
                            
			<p>
                            <label class="cardLabel" for="SupplierName">
				Supplier Name:<sup>*</sup>
			    </label>
			    &nbsp;&nbsp; 
			    <input type="text" class="text" name="CompanyName" value="{if isset($data)}{$data['CompanyName']}{/if}" id="CompanyName" />
			</p>
                        <p>
                            <label class="cardLabel" >
				Account No:<sup>*</sup>
			    </label>
			    &nbsp;&nbsp; 
			    <input type="text" class="text" name="AccountNumber" value="{if isset($data)}{$data['AccountNumber']}{/if}" id="AccountNumber" />
			</p>
                        <p>
                            <label class="cardLabel" >
				Ship to Account No:
			    </label>
			    &nbsp;&nbsp; 
			    <input type="text" class="text" name="ShipToAccountNumber" value="{if isset($data)}{$data['ShipToAccountNumber']}{/if}" id="ShipToAccountNumber" />
			</p>
                        {if isset($data)}
                         <p>
                                <label class="cardLabel" for="Status" >Status:<sup>*</sup></label>

                                &nbsp;&nbsp; 

                                    {foreach $statuses as $status}

					<span class="formRadioCheckText"  class="saFormSpan">
					    <input  type="radio" name="Status"  value="{$status.Code}" {if $data.Status eq $status.Code}checked="checked"{/if}  /><label >{$status.Name|escape:'html'}</label>&nbsp;
					</span>

                                    {/foreach}    
                            </p>
                            {/if}
                              {if isset($data)&&$activeUser=="SuperAdmin"&&$data.ApproveStatus!="Approved"}
                         <p>
                                <label class="cardLabel" for="ApproveStatus" >Account Type:<sup>*</sup></label>

                                &nbsp;&nbsp; 

                                   

					<span class="formRadioCheckText"  class="saFormSpan">
					    <input  type="radio" name="ApproveStatus"  value="Approved"   /><label >Global</label>&nbsp;
					    <input  type="radio" name="ApproveStatus"  value="NotApproved"   /><label >{$ancronym} Specific</label>&nbsp;
					</span>

                                     
                            </p>
                            {/if}
                        <p>
                            <label class="cardLabel" for="PostalCode">{$page['Labels']['postcode']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 140px;" type="text" name="PostCode" id="PostalCode" value="{if isset($data)}{$data['PostCode']}{/if}" >&nbsp;

                            <input type="submit" name="quick_find_btn" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  onclick="return false;"   value="Click to find Address" >

                        </p>
                        <p id="selectOutput" ></p>


                        <p>
                            <label class="cardLabel" for="BuildingNameNumber" >{$page['Labels']['building_name']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="BuildingNameNumber" value="{if isset($data)}{$data['BuildingNameNumber']}{/if}" id="BuildingNameNumber" >          
                        </p>

                        <p>
                            <label class="cardLabel" for="Street" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="Street" value="{if isset($data)}{$data['Street']}{/if}" id="Street" >          
                        </p>

                        <p>
                            <label class="cardLabel" for="LocalArea" >{$page['Labels']['area']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="LocalArea" value="{if isset($data)}{$data['LocalArea']}{/if}" id="LocalArea" >          
                        </p>


                        <p>
                            <label class="cardLabel" for="TownCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TownCity" value="{if isset($data)}{$data['TownCity']}{/if}" id="TownCity" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="CountryID" >{$page['Labels']['country']|escape:'html'}:</label>
                            &nbsp;&nbsp; 
                                <select name="CountryID" id="CountryID" class="text" >
                                <option value="" {if isset($data)&& $data.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $countries as $country}

                                    <option value="{$country.CountryID}" {if isset($data)&& $data.CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>
                                    
                                {/foreach}
                                
                            </select>
                                
                        </p>
                         <p>
                            <label class="cardLabel" for="Telephone" >Telephone Number:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TelephoneNo" value="{if isset($data)}{$data['TelephoneNo']}{/if}" id="TelephoneNo" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="FaxNo" >Fax Number:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="FaxNo" value="{if isset($data)}{$data['FaxNo']}{/if}" id="FaxNo" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="EmailAddress" >Email Address:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="EmailAddress" value="{if isset($data)}{$data['EmailAddress']}{/if}" id="EmailAddress" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="Website" >Website:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="Website" value="{if isset($data)}{$data['Website']}{/if}" id="Website" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="ContactName" >Contact Name:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="ContactName" value="{if isset($data)}{$data['ContactName']}{/if}" id="ContactName" >          
                        </p>
       
                               
  </div>
  <div id="tabs-2">
    <p>
                                <label ></label>
                                <span class="topText cardTopLabel" style="font-size:12px!important;color:#737373">Fields marked with a <sup>*</sup> are compulsory</span>
                            </p>
                           

                             <p>
                            <label class="cardLabel" for="DirectOrderTemplateID" >Direct Order Template :</label>
                            &nbsp;&nbsp; <select  class="text" name="DirectOrderTemplateID" value="" id="DirectOrderTemplateID" >   
                                <option></option>
                            </select>
                        </p>
                             <p>
                            <label class="cardLabel" for="CollectSupplierOrderNo" >Collect Supplier’s Order No:</label>
                            &nbsp;&nbsp; <input type="checkbox"   name="CollectSupplierOrderNo" {if isset($data) && $data['CollectSupplierOrderNo']=="Yes"}checked=checked{/if} value="Yes" id="CollectSupplierOrderNo" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="PostageChargePrompt" >Postage Charge Prompt:</label>
                            &nbsp;&nbsp; <input type="checkbox"  name="PostageChargePrompt" {if isset($data) && $data['PostageChargePrompt']=="Yes"}checked=checked{/if} value="Yes" id="PostageChargePrompt" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="DefaultPostageCharge" >Default Postage Charge:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="DefaultPostageCharge" value="{if isset($data)}{$data['DefaultPostageCharge']}{/if}" id="DefaultPostageCharge" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="DefaultCurrencyID" >Default Currency:</label>
                             &nbsp;&nbsp; 
                               <select class="text" name="DefaultCurrencyID"  id="DefaultCurrencyID" >   
                                <option value="" {if isset($data)&& $data.DefaultCurrencyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $currencylist as $currency}

                                    <option value="{$currency.CurrencyID}" {if isset($data)&& $data.DefaultCurrencyID eq $currency.CurrencyID}selected="selected"{/if}>{$currency.CurrencyName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                           
                         </p>
                        <p>
                            <label class="cardLabel" for="NormalSupplyPeriod" >Normal Supply Period:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="NormalSupplyPeriod" value="{if isset($data)}{$data['NormalSupplyPeriod']}{/if}" id="NormalSupplyPeriod" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="UsageHistoryPeriod" >UsageHistoryPeriod:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="UsageHistoryPeriod" value="{if isset($data)}{$data['UsageHistoryPeriod']}{/if}" id="UsageHistoryPeriod" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="MinimumOrderValue" >Minimum Order Value:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="MinimumOrderValue" value="{if isset($data)}{$data['MinimumOrderValue']}{/if}" id="MinimumOrderValue" >          
                        </p>
                        <p>
                            <label class="cardLabel" for="MultiplyByFactor" >Multiply By Factor Of (%):</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="MultiplyByFactor" value="{if isset($data)}{$data['MultiplyByFactor']}{/if}" id="MultiplyByFactor" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="TaxExempt" >Tax Exempt:</label>
                            &nbsp;&nbsp; <input type="checkbox"  name="TaxExempt"  {if isset($data) && $data['TaxExempt']=="Yes"}checked=checked{/if} value="Yes" id="TaxExempt" >          
                        </p>
                         <p>
                            <label class="cardLabel" for="OrderPermittedVariancePercent" >Requisition/Order Permitted Variance (%):</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="OrderPermittedVariancePercent" value="{if isset($data)}{$data['OrderPermittedVariancePercent']}{/if}" id="OrderPermittedVariancePercent" >          
                        </p>
      
  </div>
                                
</div
<hr>
                                <br>
                                <button type="submit" style="float: left" class="gplus-blue">Save</button>
                                <button type="button" onclick="$.colorbox.close();" style="float:right" class="gplus-blue">Cancel</button>
                                 </fieldset>
   </form>
    </div>
         


     
 
         