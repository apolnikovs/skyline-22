<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of TAT Defaults
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 * 
 * Changes
 * Date        Version Author                 Reason
 * 29/03/2013  1.00    Nageswara Rao Kanteti  Initial Version 
 ******************************************************************************/

class TAT extends CustomModel {
    
    private $conn;
    private $dbColumns = array('TatID', 'TatType', 'Button1', 'Button3', 'Button4', 'Button1Colour', 'Button2Colour', 'Button3Colour', 'Button4Colour');
    private $table     = "tat_defaults";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['TatID']) || !$args['TatID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
    
    
    /**
     * Description
     * 
     * This method is used for to validate TatType.
     *
     * @param interger $TatType  
     * @param interger $TatID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($TatType, $TatID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT TatID FROM '.$this->table.' WHERE TatType=:TatType AND UserID=:UserID AND TatID!=:TatID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':TatType' => $TatType, ':UserID' => $this->controller->user->UserID, ':TatID' => $TatID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['TatID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (TatType, Button1, Button3, Button4, Button1Colour, Button2Colour, Button3Colour, Button4Colour, UserID)
            VALUES(:TatType, :Button1, :Button3, :Button4, :Button1Colour, :Button2Colour, :Button3Colour, :Button4Colour, :UserID)';
        
        
        if($this->isValidAction($args['TatType'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(
                
                ':TatType' => $args['TatType'],  
                ':Button1' => $args['Button1'], 
                ':Button3' => $args['Button3'],
                ':Button4' => $args['Button4'],
                ':Button1Colour' => $args['Button1Colour'],
                ':Button2Colour' => $args['Button2Colour'],
                ':Button3Colour' => $args['Button3Colour'],
                ':Button4Colour' => $args['Button4Colour'],
                ':UserID' => $this->controller->user->UserID
                
                ));
        
        
              return array('status' => 'OK',
                        'message' => '');
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        if(isset($args['TatType']))
        {
            $sql = 'SELECT TatID, TatType, Button1, Button3, Button4, Button1Colour, Button2Colour, Button3Colour, Button4Colour FROM '.$this->table.' WHERE TatType=:TatType AND UserID=:UserID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':TatType' => $args['TatType'], ':UserID'=>$this->controller->user->UserID ));
        }   
        else
        {
            $sql = 'SELECT TatID, TatType, Button1, Button3, Button4, Button1Colour, Button2Colour, Button3Colour, Button4Colour FROM '.$this->table.' WHERE TatID=:TatID AND UserID=:UserID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':TatID' => $args['TatID'], ':UserID'=>$this->controller->user->UserID));
        }
        
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    
    
    
      /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['TatType'], $args['TatID']))
        {  
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              TatType=:TatType, Button1=:Button1, Button3=:Button3, Button4=:Button4, Button1Colour=:Button1Colour, Button2Colour=:Button2Colour, Button3Colour=:Button3Colour, Button4Colour=:Button4Colour            

              WHERE TatID=:TatID AND UserID=:UserID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        
                        ':TatType' => $args['TatType'],  
                        ':Button1' => $args['Button1'], 
                        ':Button3' => $args['Button3'],
                        ':Button4' => $args['Button4'],
                        ':Button1Colour' => $args['Button1Colour'],
                        ':Button2Colour' => $args['Button2Colour'],  
                        ':Button3Colour' => $args['Button3Colour'],
                        ':Button4Colour' => $args['Button4Colour'],
                        ':TatID' => $args['TatID'],
                        ':UserID'=>$this->controller->user->UserID  
                        
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => '');
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    
    
    
}
?>