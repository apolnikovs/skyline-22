<?php
/**
 * OneTouchSoapController.class.php
 * 
 * Soap Server for One Touch Interface
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link 
 * @version    1.16
 * 
 * Changes
 * Date        Version Author                Reason
 * 30/01/2013  1.00    Andrew J. Williams    Initial Version (Required in Trackerbase VMS Log 157)
 * 13/02/2013  1.01    Andrew J. Williams    Trackerbase VMS Log 170 - Changes to Samsung one touch booking to include Address 
 * 18/02/2013  1.02    Andrew J. Williams    Set header to XML
 * 19/02/2013  1.03    Andrew J. Williams    Issue 224 - Null skillset being passed to diary by OneTouch controller
 * 20/02/2013  1.04    Andrew J. Williams    Issue 227 - Samsung One Touch - Skillset ID Not Being returned
 * 28/02/2013  1.05    Andrew J. Williams    Issue 238 - One Touch Interface - Run Viamente after appointment insert
 * 28/02/2013  1.06    Andrew J. Williams    Issue 239 - Log in for One Touch
 * 28/02/2013  1.07    Andris Polnikovs      Changed Repair to Samsung appointment type
 * 11/03/2013  1.08    Andris Polnikovs      Added email routines
 * 13/03/2013  1.09    Andrew J. Williams    Issue 258 - Strip 0s from ASC number on CancelBooking and Stop Creation of Duplicate Jobs in Put Reservation
 * 13/03/2013  1.10    Andrew J. Williams    Corrections to send email - $emailModel = $this->loadModel('APIViamente') to $emailModel = $this->loadModel('Email')
 * 18/03/2013  1.11    Andrew J. Williams    Check SkylineJobID befor NonSkylineJobID in cancel
 * 19/03/2013  1.12    Andrew J. Williams    Only run viamente if services provider is configured to use it
 * 20/03/2013  1.13    Andrew J. Williams    Fixed not matching NetworkRefNo to NonSkylineJobs
 * 21/03/2013  1.14    Andrew J. Williams    Issue 271 - One Touch for Ancrum/South Esk
 * 25/03/2013  1.15    Andrew J. Williams    Issue 277 - One Touch upgrade for Andris
 * 02/05/2013  1.16    Andrew J. Williams    Trackerbase VMS Log 254 - One Touch Booking audit log
 ******************************************************************************/

require_once('SkylineSOAPController.class.php');
require_once('xml2array.php');

class OneTouchSoapController extends SkylineSOAPController {
    public $debug = false;                                                      /* Turn on or off debugging */
    public $ServiceProviderID;                                                  /* Used by ViamenteAPI to get the service provider */
    protected $wsdl;
    protected $serviceName;
    protected $functions = array();
    protected $sharedtns = array();

    public function __construct() {
        parent::__construct(); 

        $this->serverpath .= '/OneTouchSoap/';
        
        $this->config = $this->readConfig('application.ini');                   /* Read Application Config file. */
        $this->session = $this->loadModel('Session');                           /* Initialise Session Model */
        $this->messages = $this->loadModel('Messages');                         /* Initialise Messages Model */
        
        $this->wsdl = $this->config['Samsung']['OneTouchWsdl'];                 /* Get the rqeuires wsdl */
        
        $this->serviceName = "Samsung One Touch";
        
        $this->sharedtns = array(
                            "BOOKING_REQUEST" => array( 
                                                    array("name" => "UserName", "type" => "string", 'min' => 1, 'max' => 1),
                                                    array("name" => "Password", "type" => "string", 'min' => 1, 'max' => 1),
                                                    array("name" => "ASC_CODE", "type" => "string", 'min' => 0, 'max' => 1),
                                                    array("name" => "SERVICEORDER_NO", "type" => "string"),
                                                    array("name" => "MODEL_CODE", "type" => "string"),
					            array("name" => "PRODUCT_GROUP", "type" => "string"),
                                                    array("name" => "COUNTRY", "type" => "string"),
                                                    array("name" => "REGION", "type" => "string"),
                                                    array("name" => "POST_CODE", "type" => "string"),
                                                    array("name" => "CITY", "type" => "string"),
                                                    array("name" => "ADDRESS1", "type" => "string"),
                                                    array("name" => "ADDRESS2", "type" => "string")
					           ),
                            "ArrayOfAVAILABILITY_LIST" => array( 
                                                    array("name" => "AVAILABLE_TIMESLOTS", "type" => "tns:AVAILABLE_TIMESLOTS", 'min' => 0, 'max' => 'unbounded'),
					           ),
                            "AVAILABLE_TIMESLOTS" => array( 
                                                    array("name" => "TIME_SLOT_TYPE", "type" => "string", 'min' => 1, 'max' => 1),
                                                    array("name" => "TIME_SLOT_ID", "type" => "int", 'min' => 1, 'max' => 1),
                                                    array("name" => "AVAILABLE_DATE", "type" => "string", 'min' => 1, 'max' => 1),
					            array("name" => "AVAILABLE_TIME_FR", "type" => "string", 'min' => 1, 'max' => 1),
                                                    array("name" => "AVAILABLE_TIME_TO", "type" => "string", 'min' => 1, 'max' => 1),
                                                    array("name" => "AVAILABILITY", "type" => "int", 'min' => 1, 'max' => 1)
					           ),
                            "TIME_SLOT_CHOSEN" => array( 
                                                    array("name" => "TIME_SLOT_TYPE", "type" => "string", 'min' => 0, 'max' => 1),
                                                    array("name" => "TIME_SLOT_ID", "type" => "int"),
                                                    array("name" => "AVAILABLE_DATE", "type" => "string"),
					            array("name" => "AVAILABLE_TIME_FR", "type" => "string"),
                                                    array("name" => "AVAILABLE_TIME_TO", "type" => "string")
					           ),
                            "RETURN_MESSAGE" => array( 
                                                    array("name" => "RETURN_TYPE", "type" => "string", 'min' => 1, 'max' => 1),
                                                    array("name" => "ERR_CODE", "type" => "int", 'min' => 1, 'max' => 1),
                                                    array("name" => "ERR_MSG", "type" => "string")
					           ),
                            "CANCEL_REQUEST" => array( 
                                                    array("name" => "UserName", "type" => "string", 'min' => 1, 'max' => 1),
                                                    array("name" => "Password", "type" => "string", 'min' => 1, 'max' => 1),
                                                    array("name" => "ASC_CODE", "type" => "string", 'min' => 0, 'max' => 1),
                                                    array("name" => "SERVICEORDER_NO", "type" => "string")
					           )
                                );

	$this->functions[] = array("funcName" => "getAvailability",
                             "doc" => "Samsung will request to see the availability of appointment lists over the next 6 days, using the following schema.",
                             "inputParams" => array(
                                 array('name' => 'BOOKING_REQUEST', 'type' => 'tns:BOOKING_REQUEST')
                                 ),
                             "outputParams" => array(
                                                     array("name" => "RETURN_MESSAGE", "type" => "tns:RETURN_MESSAGE"),
                                                     array("name" => "AVAILABILITY_LIST", "type" => "tns:ArrayOfAVAILABILITY_LIST", 'min' => 0, 'max' => 1)
                                                    ),
                             "soapAddress" => $this->serverpath
                             );
        
        $this->functions[] = array("funcName" => "putReservation",
                             "doc" => "Record the reservation details",
                             "inputParams" => array(
                                 array('name' => 'BOOKING_REQUEST', 'type' => 'tns:BOOKING_REQUEST'),
                                 array('name' => 'TIME_SLOT_CHOSEN', 'type' => 'tns:TIME_SLOT_CHOSEN'),
                                 ),
                             "outputParams" => array(
                                                     array("name" => "RETURN_MESSAGE", "type" => "tns:RETURN_MESSAGE")
                                                    ),
                             "soapAddress" => $this->serverpath
                             );
        
        $this->functions[] = array("funcName" => "putCancelBooking",
                             "doc" => "Cancel a booking",
                             "inputParams" => array(
                                 array('name' => 'CANCEL_REQUEST', 'type' => 'tns:CANCEL_REQUEST')
                                 ),
                             "outputParams" => array(
                                                     array("name" => "RETURN_MESSAGE", "type" => "tns:RETURN_MESSAGE")
                                                    ),
                             "soapAddress" => $this->serverpath
                             );
    }
        
    /**
     * getAvailability
     * 
     * Samsung will request to see the availability of appointment lists over 
     * the next 6 days.
     * 
     * @param string $xml   Parameters in xml
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getAvailability($xml) { 
        $allocation_reassignmnet = $this->loadModel('AllocationReassignment');
        $appointment_type_model = $this->loadModel('AppointmentType');
        $diary_model = $this->loadModel('Diary');
        $models_model = $this->loadModel('Models');
        $one_touch_audit_model = $this->loadModel('OneTouchAudit');
        $service_providers_model = $this->loadModel('ServiceProviders');
        $skillset_model = $this->loadModel('Skillset');
        $skyline_model = $this->loadModel('Skyline');
        $unit_type_manufacturer_model = $this->loadModel('UnitTypeManufacturer');
        $users_model = $this->loadModel('Users');
        
        header("Content-Type: application/xml; charset=utf-8"); 
        
        $audit = array();                                                       /* Create Array to store audit values */
        
        if ($this->debug) $this->log($xml,'one_touch_');
        
        $audit['Type'] = 'getAvailability';
        $audit['ActionDate'] = date('Y-m-d H:i:s');
        $audit['IPAddress'] =  getenv('REMOTE_ADDR');
        
        $data['BOOKING_REQUEST'] = (array) $xml->BOOKING_REQUEST;
        
        $audit['ServiceOrder'] = (isset($data['BOOKING_REQUEST']['SERVICEORDER_NO']) ? $data['BOOKING_REQUEST']['SERVICEORDER_NO'] : '');
        
        if (! isset($data['BOOKING_REQUEST']['UserName']) ) {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 001;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'Unauthorised';
            $r['AVAILABILITY_LIST'] = array();
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        $auth = $users_model->checkUser($data['BOOKING_REQUEST']['UserName'], $data['BOOKING_REQUEST']['Password']);
        
        $audit['Username'] = $data['BOOKING_REQUEST']['UserName'];

        if ( $auth->AuthOK == 0) {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 001;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'Unauthorised';
            $r['AVAILABILITY_LIST'] = array();
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        if ($this->debug) $this->log($data,'one_touch_');
        
        /*$r = array('RETURN_MESSAGE' => array('RETURN_TYPE' => 'F'));

        $rObj = $this->arrayToObject($r);
         $this->log($rObj);
        return($rObj);*/
        
        //$response = '<?xml version="1.0" encoding="utf-8">';
        //$response .= "\n<RESPONSE>";
 
        if (! empty($data['BOOKING_REQUEST']['ASC_CODE'])) {                      /* Check if we have Account number (ASC code) */
            $network_service_provider_array = $skyline_model->getNetworkServiceProviderAccountNo(intval($data['BOOKING_REQUEST']['ASC_CODE']));     /* Yes - get nsp records */
            $spId = $network_service_provider_array['ServiceProviderID'];       /* We now know ServiceProviderID */
            $audit['ServiceProviderID'] = $spId;
            
            /* Start changes - Issue 271 - One Touch for Ancrum/South Esk */
            /* https://bitbucket.org/pccs/skyline/issue/271/one-touch-for-ancrum-south-esk */
            if ( isset($data['BOOKING_REQUEST']['POST_CODE']) ) {               /* If we have a podtcode set */
                $area = trim(substr($data['BOOKING_REQUEST']['POST_CODE'], 0, -3));   /* Get UK postode area */
                if($spId!=""){
                $secondarySpId = $allocation_reassignmnet->getSecondaryServiceProvider($spId, $area);       /* Get secondary servive provider */
                }else{
                   $secondarySpId=null; 
                }
                if (! is_null($secondarySpId)) {                                 /* If we have secondary service provider */
                    $spId = $secondarySpId;                                     /* Use it */
                } /* fi ! is_null $secondarySpId */
                $audit['Postcode'] = $data['BOOKING_REQUEST']['POST_CODE'];
            } /* fi isset($data['BOOKING_REQUEST']['POST_CODE'] */
            /* End changes - Issue 271 - One Touch for Ancrum/South Esk */
            
            if ( is_null($spId)) {
                $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
                $r['RETURN_MESSAGE']['ERR_CODE'] = 003;
                $r['RETURN_MESSAGE']['ERR_MSG'] = 'ASC does not exist in the Skyline diary system';
                $r['AVAILABILITY_LIST'] = array();
                if ($this->debug) $this->log($r,'one_touch_');
                $rObj = $this->arrayToObject($r);
                if ($this->debug) $this->log($rObj,'one_touch_');
                return($rObj);
            }
            if ($this->debug) $this->log($network_service_provider_array,'one_touch_');
        } else {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 003;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'ASC does not exist in the Skyline diary system';
            $r['AVAILABILITY_LIST'] = array();
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        if ($service_providers_model->getServiceCentreOnlineDiary($spId) == 'In-Active') {      /* If noy using online diary return error */
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 002;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'ASC does not use online diary';
            $r['AVAILABILITY_LIST'] = array();
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        if (! isset($data['BOOKING_REQUEST']['MODEL_CODE'])) {                  /* Check if model code passed */           
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 004;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'No MODEL_CODE passed';
            $r['AVAILABILITY_LIST'] = array();
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        if (! isset($data['BOOKING_REQUEST']['PRODUCT_GROUP'])) {               /* Check if product group passed */            
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 008;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'No PRODUCT_GROUP passed';
            $r['AVAILABILITY_LIST'] = array();
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        $utId = $unit_type_manufacturer_model->getUnitTypeIdByProductGroup($data['BOOKING_REQUEST']['PRODUCT_GROUP']);    /* Can't be found try to look up product group*/
        if ($this->debug) $this->log("getAvailability : Unit type from Model No : $utId",'one_touch_');
        if ( is_null($utId) ) {                                                 /* Check if the retuned model is null (can't be found) */
            $utId = $models_model->getUnitTypeIdFromNo($data['BOOKING_REQUEST']['MODEL_CODE']);     /* Look up model number in Models table */
            if ($this->debug) $this->log("getAvailability : Unit type from Product Group : $utId",'one_touch_');
            if ( is_null($utId) || (intval($utId) == 1) ) {                                             /* Still can't be found */
                $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
                $r['RETURN_MESSAGE']['ERR_CODE'] = 007;
                $r['RETURN_MESSAGE']['ERR_MSG'] = 'Product group not recognised';
                $r['AVAILABILITY_LIST'] = array();
                if ($this->debug) $this->log($r,'one_touch_');
                $rObj = $this->arrayToObject($r);
                if ($this->debug) $this->log($rObj,'one_touch_');
                return($rObj);
            }
        }
        $audit['ProductGroup'] = $data['BOOKING_REQUEST']['PRODUCT_GROUP'];
        
        if ( isset($data['BOOKING_REQUEST']['MODEL_CODE']) ) {
            $audit['ModelID'] = $models_model->getModelIdFromNo($data['BOOKING_REQUEST']['MODEL_CODE']);
            $audit['ModelName'] = $data['BOOKING_REQUEST']['MODEL_CODE'];
        }
        
        $appTypeId = $appointment_type_model->getIdFromType('Samsung');          /* Get the appointment ID for reapair */
        if ($this->debug) $this->log("getAvailability : Appointment Type ID : $appTypeId",'one_touch_');
        
        $ssId = $skillset_model->getIdByUnitTypeAppointmentType($appTypeId, $utId);
        if ($this->debug) $this->log("getAvailability : Skillset ID : $ssId",'one_touch_');
        
        if ( is_null($ssId) ) {                                                 /* Still can't be found */
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 006;
            $r['RETURN_MESSAGE']['ERR_MSG'] = "Can't calculated skillset from given Unit Type for Appointment Type reapair";
            $r['AVAILABILITY_LIST'] = array();
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        $address = '';
        if(isset($data['BOOKING_REQUEST']['ADDRESS1'])) {
            $audit['Address2'] = $data['BOOKING_REQUEST']['ADDRESS2'];
            $address .= $data['BOOKING_REQUEST']['ADDRESS1'].' ';
        }
        if(isset($data['BOOKING_REQUEST']['ADDRESS2'])) {
            $audit['Address2'] = $data['BOOKING_REQUEST']['ADDRESS2'];
            $address .= $data['BOOKING_REQUEST']['ADDRESS2'].' ';
        }
        if(isset($data['BOOKING_REQUEST']['ADDRESS3'])) {
            $audit['Address3'] = $data['BOOKING_REQUEST']['ADDRESS3'];
            $address .= $data['BOOKING_REQUEST']['ADDRESS3'].' ';
        }
        if(isset($data['BOOKING_REQUEST']['CITY'])) {
            $audit['City'] = $data['BOOKING_REQUEST']['CITY'];
            $address .= $data['BOOKING_REQUEST']['CITY'].' ';
        }
        if(isset($data['BOOKING_REQUEST']['COUNTRY']))  {
            $audit['Country'] = $data['BOOKING_REQUEST']['COUNTRY'];
            $address .= $data['BOOKING_REQUEST']['COUNTRY'].' ';
        }
        if(isset($data['BOOKING_REQUEST']['REGION']))  {
            $audit['Region'] = $data['BOOKING_REQUEST']['REGION'];
        }
        
        $slotsLeft =  $diary_model->getSlotsForSamsung($spId, $ssId, $data['BOOKING_REQUEST']['POST_CODE'], 7,  $address);       
        if ($this->debug) $this->log($slotsLeft,'one_touch_');
        ///Andris change for E010 - blank array
        
        $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'S';
        $r['RETURN_MESSAGE']['ERR_CODE'] = 0000;
        
        
        $r['AVAILABILITY_LIST'] = array();
        
        $slotAvailiability = array();
        
        $n = 0;
        $notempty=false;
        if ( isset($slotsLeft) && is_array($slotsLeft) ) {
            foreach ($slotsLeft as $slot) {
                $dateDiff = ceil(abs( (time() - strtotime($slot['AppointmentDate'])) /(60*60*24) ));    /*Difference in days bewteen slot and today */
                $ampm = ( $slot['TimeFrom'] == '080000' ) ? $ampm = 'AM' : $ampm = 'PM';                 /* Slot is AM or PM */
                $auditField = "Day{$dateDiff}{$ampm}Availability";                  /* Use above to calculate audit field name (See VMS Trackerbase Log 254 */

                if ( $slot['SlotsLeft'] > 0 ) {                                     /* If there is avilailiability for this slot, output a record */
                    $notempty=true;
                    
                    $slotRec['TIME_SLOT_TYPE'] = 'B';
                    $slotRec['TIME_SLOT_ID'] = $slot['DiaryAllocationID'];
                    $slotRec['AVAILABLE_DATE'] = $slot['AppointmentDate'];
                    $slotRec['AVAILABLE_TIME_FR'] = $slot['TimeFrom'];
                    $slotRec['AVAILABLE_TIME_TO'] = $slot['TimeTo'];
                    $slotRec['AVAILABILITY'] = 1;
                    $slotAvailiability[$n] = $this->arrayToObject($slotRec);
                    $n++;
                    $audit[$auditField] = 'Yes';
                } else {
                    $audit[$auditField] = 'No';
                }
            }
        }else
        {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
        $r['RETURN_MESSAGE']['ERR_CODE'] = 10;
         $r['RETURN_MESSAGE']['ERR_MSG'] = "No availability for given criteria";
        }
        
        
        if(!$notempty){
              $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
        $r['RETURN_MESSAGE']['ERR_CODE'] = 10;
         $r['RETURN_MESSAGE']['ERR_MSG'] = "No availability for given criteria";
        }
        $this->log($r,'one_touch_');
        

        $rObj = $this->arrayToObject($r);

        $rObj->AVAILABILITY_LIST = $slotAvailiability;
        if ($this->debug) $this->log($rObj,'one_touch_');
        $one_touch_audit_model->create($audit);
        return($rObj);
    }    
    
    /**
     * putReservation
     * 
     * Record the reservation details
     * 
     * @param string $xml   Parameters in xml
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function putReservation($xml) {
        if ($this->debug) $this->log('Starting putReservation','one_touch_');
        
        $allocation_reassignmnet = $this->loadModel('AllocationReassignment');
        $api_appointments_model = $this->loadModel('APIAppointments');
        $appointment_model = $this->loadModel('Appointment');
        $appointment_type_model = $this->loadmodel('AppointmentType');
        $diary_model = $this->loadModel('Diary');
        $job_model = $this->loadModel('Job');
        $models_model = $this->loadModel('Models');
        $non_skyline_job_model = $this->loadModel('NonSkylineJob');
        $one_touch_audit_model = $this->loadModel('OneTouchAudit');
        $service_providers_model = $this->loadModel('ServiceProviders');
        $service_provider_skills_set_model = $this->loadModel('ServiceProviderSkillsSet');
        $skillset_model = $this->loadModel('Skillset');
        $skyline_model = $this->loadModel('Skyline');
        $unit_type_manufacturer_model = $this->loadModel('UnitTypeManufacturer');
        $users_model = $this->loadModel('Users');

        $audit = array();                                                       /* Create Array to store audit values */
        
        $data['BOOKING_REQUEST'] = (array) $xml->BOOKING_REQUEST;   
        $data['TIME_SLOT_CHOSEN'] = (array) $xml->TIME_SLOT_CHOSEN; 
        if ($this->debug) $this->log($data,'one_touch_');                

        $audit['Type'] = 'putReservation';
        $audit['ActionDate'] = date('Y-m-d H:i:s');
        $audit['IPAddress'] =  getenv('REMOTE_ADDR');
        $audit['ServiceOrder'] = (isset($data['BOOKING_REQUEST']['SERVICEORDER_NO']) ? $data['BOOKING_REQUEST']['SERVICEORDER_NO'] : '');
        
        if (! isset($data['BOOKING_REQUEST']['UserName']) ) {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 001;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'Unauthorised';

            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        $auth = $users_model->checkUser($data['BOOKING_REQUEST']['UserName'], $data['BOOKING_REQUEST']['Password']);

        if ( $auth->AuthOK == 0) {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 001;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'Unauthorised';
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        $audit['Username'] = $data['BOOKING_REQUEST']['UserName'];
        
        if (! empty($data['BOOKING_REQUEST']['ASC_CODE'])) {       /* Check if we have Account number (ASC code) */
            $network_service_provider_array = $skyline_model->getNetworkServiceProviderAccountNo(intval($data['BOOKING_REQUEST']['ASC_CODE']));     /* Yes - get nsp records */
            if ($this->debug) $this->log($network_service_provider_array,'one_touch_');
            if (count($network_service_provider_array) == 0) {
                $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
                $r['RETURN_MESSAGE']['ERR_CODE'] = 003;
                $r['RETURN_MESSAGE']['ERR_MSG'] = 'ASC does not exist in the Skyline diary system';
                if ($this->debug) $this->log($r,'one_touch_');
                $rObj = $this->arrayToObject($r);
                if ($this->debug) $this->log($rObj,'one_touch_');
                return($rObj);
            }
            $spId = $network_service_provider_array['ServiceProviderID'];       /* We now know ServiceProviderID */
            $audit['ServiceProviderID'] = $spId;
            $nId = $network_service_provider_array['NetworkID'];                /* and Network ID */
            
            /* Start changes - Issue 271 - One Touch for Ancrum/South Esk */
            /* https://bitbucket.org/pccs/skyline/issue/271/one-touch-for-ancrum-south-esk */
            if ( isset($data['BOOKING_REQUEST']['POST_CODE']) ) {               /* If we have a podtcode set */
                $area = trim(substr($data['BOOKING_REQUEST']['POST_CODE'], 0, -3));   /* Get UK postode area */
                if($spId!=""){
                $secondarySpId = $allocation_reassignmnet->getSecondaryServiceProvider($spId, $area);       /* Get secondary servive provider */
                }else{
                   $secondarySpId=null; 
                }
                if (! is_null($secondarySpId)) {                                /* If we have secondary service provider */
                    $spId = $secondarySpId;                                     /* Use it */
                    if ($this->debug) $this->log("putReservation : Secondary Service Provider : $spId",'one_touch_');
                } /* fi ! is_null $secondarySpId */
                $audit['Postcode'] = $data['BOOKING_REQUEST']['POST_CODE'];
            } /* fi isset($data['BOOKING_REQUEST']['POST_CODE'] */
            /* End changes - Issue 271 - One Touch for Ancrum/South Esk */
        } else {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 004;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'ASC not passed';
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        $this->ServiceProviderID = $spId;
        
        if ($service_providers_model->getServiceCentreOnlineDiary($spId) == 'In-Active') {      /* If not using online diary return error */
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 002;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'ASC does not use online diary';
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        if (! isset($data['BOOKING_REQUEST']['MODEL_CODE'])) {                  /* Check if model code passed */           
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 009;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'No MODEL_CODE passed';
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        if (! isset($data['BOOKING_REQUEST']['PRODUCT_GROUP'])) {               /* Check if product group passed */            
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 008;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'No PRODUCT_GROUP passed';
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
 
        
       $utId = $models_model->getUnitTypeIdFromNo($data['BOOKING_REQUEST']['MODEL_CODE']);     /* Look up model number in Models table */
        if ($this->debug) $this->log("getAvailability : Unit type from Model No : $utId",'one_touch_');
        if ( is_null($utId) || (intval($utId) == 1) ) {                                                 /* Check if the retuned model is null (can't be found) */
            $utId = $unit_type_manufacturer_model->getUnitTypeIdByProductGroup($data['BOOKING_REQUEST']['PRODUCT_GROUP']);    /* Can't be found try to look up product group*/
            if ($this->debug) $this->log("getAvailability : Unit type from Product Group : $utId",'one_touch_');
            if ( is_null($utId) ) {                                             /* Still can't be found */
                $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
                $r['RETURN_MESSAGE']['ERR_CODE'] = 007;
                $r['RETURN_MESSAGE']['ERR_MSG'] = 'Product group not recognised';
                if ($this->debug) $this->log($r,'one_touch_');
                $rObj = $this->arrayToObject($r);
                if ($this->debug) $this->log($rObj,'one_touch_');
                return($rObj);
            }
        }
        $audit['ProductGroup'] = $data['BOOKING_REQUEST']['PRODUCT_GROUP'];
        
        if ( isset($data['BOOKING_REQUEST']['MODEL_CODE']) ) {
            $audit['ModelID'] = $models_model->getModelIdFromNo($data['BOOKING_REQUEST']['MODEL_CODE']);
            $audit['ModelName'] = $data['BOOKING_REQUEST']['MODEL_CODE'];
        }
        
        $appTypeId = $appointment_type_model->getIdFromType('Samsung');          /* Get the appointment ID for reapair */
        if ($this->debug) $this->log("putReservation : Appointment Type ID : $appTypeId",'one_touch_');
        
        $ssId = $skillset_model->getIdByUnitTypeAppointmentType($appTypeId, $utId);
        if ($this->debug) $this->log("putReservation: Skillset ID : $ssId",'one_touch_');
        
        if ( is_null($ssId) ) {                                                 /* Still can't be found */
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 006;
            $r['RETURN_MESSAGE']['ERR_MSG'] = "Can't calculated skillset from given Unit Type for Appointment Type reapair";
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        if ( 
                ( ! isset($data['TIME_SLOT_CHOSEN']['TIME_SLOT_ID']) )
                || ( ! isset($data['TIME_SLOT_CHOSEN']['AVAILABLE_TIME_FR']) )
                || ( ! isset($data['TIME_SLOT_CHOSEN']['AVAILABLE_TIME_TO']) )
                ) {        
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 011;
            $r['RETURN_MESSAGE']['ERR_MSG'] = "No diary timeslot parameters passed";
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }

        
        $timeslotDate = date( 'Y-m-d', strtotime($data['TIME_SLOT_CHOSEN']['AVAILABLE_DATE']));
        $timeslotFrom = date( 'H:i:s', strtotime($data['TIME_SLOT_CHOSEN']['AVAILABLE_TIME_FR']));
        $timeslotTo = date( 'H:i:s', strtotime($data['TIME_SLOT_CHOSEN']['AVAILABLE_TIME_TO']));
        
        $jId = $job_model->getIdFromNetworkRef($data['BOOKING_REQUEST']['SERVICEORDER_NO']);
        if(is_null($jId)) {                                                     /* Checkif we can find a JobID */
            $nsjId = $non_skyline_job_model->getIdFromNetworkRef($nId, $data['BOOKING_REQUEST']['SERVICEORDER_NO']);  /* No, so check if it is an existing non skyline job */
            if ( is_null($nsjId) ) {                                            /* No, so we must create it */
                $nsjAddress1 = (isset($data['BOOKING_REQUEST']['ADDRESS2']) ? $data['BOOKING_REQUEST']['ADDRESS1'].', ' : null)
                               .(isset($data['BOOKING_REQUEST']['ADDRESS1']) ? $data['BOOKING_REQUEST']['ADDRESS2'] : null);
                $non_skyline_job_fields = array(                                    /* No so we need to put inro None Skyline Job ID */
                                                'NetworkRefNo' => (isset($data['BOOKING_REQUEST']['SERVICEORDER_NO']) ? $data['BOOKING_REQUEST']['SERVICEORDER_NO'] : ''),
                                                'NetworkID' => $nId,
                                                'ServiceProviderID' => $spId,
                                                'CustomerTitle' => 'Samsung',
                                                'CustomerSurname' => 'OneTouch',
                                                'CustomerAddress1' => $nsjAddress1,
                                                'CustomerAddress2' => (isset($data['BOOKING_REQUEST']['ADDRESS3']) ? $data['BOOKING_REQUEST']['ADDRESS3'] : null),
                                                'CustomerAddress3' => (isset($data['BOOKING_REQUEST']['CITY']) ? $data['BOOKING_REQUEST']['CITY'] : null),
                                                'CustomerAddress4' => (isset($data['BOOKING_REQUEST']['REGION']) ? $data['BOOKING_REQUEST']['REGION'] : null),
                                                'PostCode' => (isset($data['BOOKING_REQUEST']['POST_CODE']) ? $data['BOOKING_REQUEST']['POST_CODE'] : null),
                                                'ModelNumber' => (isset($data['BOOKING_REQUEST']['MODEL_CODE']) ? $data['BOOKING_REQUEST']['MODEL_CODE'] : null)
                                               );

                $nsjCr = $non_skyline_job_model->create($non_skyline_job_fields);

                $nsjId = $nsjCr['id'];
            }/* fi is_null $nsjId */
            
            $appointment_fields['NonSkylineJobID'] = $nsjId;
            
            if ($this->debug) $this->log( "putReservation : NonSkylineJobID = {$nsjId}" ,'one_touch_');
        } else { /* Existing Skyline Job ID */
            $appointment_fields['JobID'] = $jId;
            if ($this->debug) $this->log( "putReservation : SJobID = $jId" ,'one_touch_');
        } /* fi is_null $jId */
        
        if(isset($data['BOOKING_REQUEST']['ADDRESS1'])) {
            $audit['Address2'] = $data['BOOKING_REQUEST']['ADDRESS2'];;
        }
        if(isset($data['BOOKING_REQUEST']['ADDRESS2'])) {
            $audit['Address2'] = $data['BOOKING_REQUEST']['ADDRESS2'];
        }
        if(isset($data['BOOKING_REQUEST']['ADDRESS3'])) {
            $audit['Address3'] = $data['BOOKING_REQUEST']['ADDRESS3'];
        }
        if(isset($data['BOOKING_REQUEST']['CITY'])) {
            $audit['City'] = $data['BOOKING_REQUEST']['CITY'];
        }
        if(isset($data['BOOKING_REQUEST']['COUNTRY']))  {
            $audit['Country'] = $data['BOOKING_REQUEST']['COUNTRY'];
        }
        if(isset($data['BOOKING_REQUEST']['REGION']))  {
            $audit['Region'] = $data['BOOKING_REQUEST']['REGION'];
        }
        
        $appointment_fields['AppointmentDate'] = $timeslotDate;
        $audit['AvailiableDate'] = $timeslotDate;
        $appointment_fields['AppointmentStartTime'] = $timeslotFrom;
        $appointment_fields['AppointmentEndTime'] = $timeslotTo;
        $appointment_fields['AppointmentTime'] = $diary_model->getAppointmentTimeFromDiaryAllocation($data['TIME_SLOT_CHOSEN']['TIME_SLOT_ID']);
        $appointment_fields['ServiceProviderID'] = $spId;
        $appointment_fields['DiaryAllocationID'] = $data['TIME_SLOT_CHOSEN']['TIME_SLOT_ID'];
        $audit['DiaryAllocationID'] = $data['TIME_SLOT_CHOSEN']['TIME_SLOT_ID'];
        $appointment_fields['AppointmentType'] = 'Samsung';
        $appointment_fields['SBAppointID'] = 0;
        $appointment_fields['BookedBy'] = 'Samsung One Touch';
        
        $spss = $service_provider_skills_set_model->getServiceProviderSkillsetFromIds($ssId,$spId);
         
        if (! is_null($spss) ) {
            $appointment_fields['MenRequired'] = $spss['EngineersRequired'];
            $appointment_fields['ServiceProviderSkillsetID'] = $spss['ServiceProviderSkillsetID'];
            $appointment_fields['Duration'] = $spss['ServiceDuration'];
        }
        
        /* Begin Issue 277 - https://bitbucket.org/pccs/skyline/issue/277/onetouch-upgrade */
        $engId = $diary_model->getOneTouchForcedEngineers(
                                                            $data['TIME_SLOT_CHOSEN']['TIME_SLOT_ID'],
                                                            $ssId,
                                                            (isset($data['BOOKING_REQUEST']['POST_CODE']) ? $data['BOOKING_REQUEST']['POST_CODE'] : null),
                                                            $spId,
                                                            $timeslotDate
                                                          );
        
        if ( $engId != false) {
            $appointment_fields['ServiceProviderEngineerID'] = $engId;
            $appointment_fields['ForceEngineerToViamente'] = 1;
            
            $sp = $diary_model->getAllServiceProviderInfo($spId);
            
            if ( $sp['ViamenteRunType'] == "CurrentEngineer" ) {
                $appointment_fields['TempSpecified'] = "Yes";
            } else {
                $appointment_fields['TempSpecified'] = "No";
            }
        }
        /* End Issue 277 */
        
        if ( ! $diary_model->isSlotForToday($appointment_fields['DiaryAllocationID'], $appointment_fields['AppointmentDate']) ) {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 012;
            $r['RETURN_MESSAGE']['ERR_MSG'] = "TIME_SLOT_ID {$data['TIME_SLOT_CHOSEN']['TIME_SLOT_ID']} does not match date {$appointment_fields['AppointmentDate']}";
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
        }
        
        if ($this->debug) $this->log( $appointment_fields ,'one_touch_');
        
        $aResp = $appointment_model->create($appointment_fields);
        if ( isset($aResp['appointmentId']) && isset($secondarySpId) ) {
            $api_appointments_model->PutAppointmentDetails($aResp['appointmentId']);
        } /* fi isset $aResp['appointmentId'] && isset $secondarySpId */
     
        /* Run viamente if service provider uses it */
        if ($service_providers_model->getServiceCentreDiaryType($spId) == "FullViamente") {
            $viamente_model = $this->loadModel('APIViamente');
            $formated_date =  date( 'Y-m-d', strtotime($timeslotDate) );
            $eIds = $diary_model->getAssignedEngineers($formated_date, $data['BOOKING_REQUEST']['POST_CODE'], $spId);
            if ($this->debug) $this->log( "putReservation : Engineer Ids = ".  var_export($eIds, true) ,'one_touch_');
            $ret = $viamente_model->OptimiseRoute($spId, $formated_date, $eIds);

            if(isset($ret->unreachedWaypointNames)&&sizeof($ret->unreachedWaypointNumbers)>0){
                $diary_model->setUnreached($ret->unreachedWaypointNames);
            }

            if(isset($ret->routes)){
                $diary_model->updateEngineerWorkload($ret->routes,$formated_date,$spId);
            }
        }
        
        if ($this->debug) $this->log( $aResp ,'one_touch_');
        
        if( $aResp['status'] != 'SUCCESS' ) {                                   /* Do we have an appointment id ? */
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 013;
            $r['RETURN_MESSAGE']['ERR_MSG'] = "Can not create appointment";
        } else {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'S';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 0000;
            
            /* Begin Andris Change - Send E-Mail */
            
            $spinfo=$diary_model->getAllServiceProviderInfo($spId);
            if($spinfo['EmailAppBooking']=='Yes'){ 
                
            $ToEmail  = $spinfo['ServiceManagerEmail'];
            $CCEmails = $spinfo['AdminSupervisorEmail'];
         
           // $ToEmail="a.polnikovs@pccsuk.com";
            $BCCEmails="a.polnikovs@pccsuk.com;r.perry@pccsuk.com;j.berry@pccsuk.com;n.wrighting@pccsuk.com".';'.$spinfo['DiaryAdministratorEmail'];
            $emailModel = $this->loadModel('Email');//sending email if appointment created
            $this->log($aResp['appointmentId'],"emailBookingLog");
            $this->log($ToEmail,"emailBookingLog");
            $this->log($CCEmails,"emailBookingLog");
            $this->log($BCCEmails,"emailBookingLog");
            
            
            $emailModel->sendAppointmentEmail($aResp['appointmentId'], 'Booked', $ToEmail, $CCEmails, $BCCEmails);
            
            //$ToEmail=$spinfo['DiaryAdministratorEmail'];
           // $emailModel->sendAppointmentEmail($aResp['appointmentId'], 'Booked', $ToEmail, false,false);
            
            }
            /* End Andris Change - Send E-Mail */
        }
        
        if ($this->debug) $this->log($r,'one_touch_');
        $rObj = $this->arrayToObject($r);
        if ($this->debug) $this->log($rObj,'one_touch_');
        $one_touch_audit_model->create($audit);
        return($r);        
    }
    
    /**
     * putCancelBooking
     * 
     * Cancel a booking
     * 
     * @param string $xml   Parameters in xml
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function putCancelBooking($xml) {
        $api_appointments_model = $this->loadModel('APIAppointments');
        $appointment_model = $this->loadModel('Appointment');
        $diary_model = $this->loadModel('Diary');
        $job_model = $this->loadModel('Job');
        $non_skyline_job_model = $this->loadModel('NonSkylineJob');
        $one_touch_audit_model = $this->loadModel('OneTouchAudit');
        $service_providers_model = $this->loadModel('ServiceProviders');
        $skyline_model = $this->loadModel('Skyline');
        $users_model = $this->loadModel('Users');
        
        $audit = array();                                                       /* Create Array to store audit values */
        
        if ($this->debug) $this->log($xml,'one_touch_');
        
        $audit['Type'] = 'putCancelJob';
        $audit['ActionDate'] = date('Y-m-d H:i:s');
        $audit['IPAddress'] =  getenv('REMOTE_ADDR');
        
        $data['CANCEL_REQUEST'] = (array) $xml->CANCEL_REQUEST;
               
        if (! isset($data['CANCEL_REQUEST']['UserName']) ) {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 001;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'Unauthorised';
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        $auth = $users_model->checkUser($data['CANCEL_REQUEST']['UserName'], $data['CANCEL_REQUEST']['Password']);

        if ( $auth->AuthOK == 0) {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 001;
            $r['RETURN_MESSAGE']['ERR_MSG'] = 'Unauthorised';
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        $audit['Username'] = $data['CANCEL_REQUEST']['UserName'];

        $acNo = ( isset($data['CANCEL_REQUEST']['ASC_CODE']) ? intval($data['CANCEL_REQUEST']['ASC_CODE']) : '' );
        //$acNo = ( isset($data['CANCEL_REQUEST']['ASC_CODE']) ? $data['CANCEL_REQUEST']['ASC_CODE'] : '' );
	$nrNo  = ( isset($data['CANCEL_REQUEST']['SERVICEORDER_NO']) ? $data['CANCEL_REQUEST']['SERVICEORDER_NO'] : '' );
        $audit['ServiceOrder'] = (isset($data['CANCEL_REQUEST']['SERVICEORDER_NO']) ? $data['CANCEL_REQUEST']['SERVICEORDER_NO'] : '');
        
        if ($this->debug) $this->log("putCancelBooking: ASC_CODE = $acNo",'one_touch_');
        if ($this->debug) $this->log("putCancelBooking: SERVICEORDER_NO (Network Ref No) = $nrNo",'one_touch_');
        
        $network_service_provider = $skyline_model->getNetworkServiceProviderAccountNo($acNo);
        
        if (count($network_service_provider) == 0) {
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 003;
            $r['RETURN_MESSAGE']['ERR_MSG'] = "ASC Code does not exist on Skyline";
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
        }
        
        $nId = $network_service_provider['NetworkID'];
        if ($this->debug) $this->log("putCancelBooking: NetworkID = $nId",'one_touch_');
        $audit['ServiveProviderID'] = $network_service_provider['ServiceProviderID'];
        
        $jId = $job_model->getIdFromNetworkRef($nrNo);      /* Check if it is a non skyline job */
        
        if ( is_null($jId) ) {
            unset($jId);
            $nsjId = $non_skyline_job_model->getIdFromNetworkRef($nId, $nrNo);
            
            if ( is_null($nsjId) ) {                                               /* Check we have a job id */
                $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
                $r['RETURN_MESSAGE']['ERR_CODE'] = 014;
                $r['RETURN_MESSAGE']['ERR_MSG'] = "Service order does not exist";
                if ($this->debug) $this->log($r,'one_touch_');
                $rObj = $this->arrayToObject($r);
                if ($this->debug) $this->log($rObj,'one_touch_');
                return($rObj);
            } else {
                if ($this->debug) $this->log("putCancelBooking: NonSkylineJobID = $nsjId",'one_touch_');
                $app = $appointment_model->getNextAppointmentNonSkyline($nsjId);
            } /* fi is_null $nsjId */
        } else {
            if ($this->debug) $this->log("putCancelBooking: JobID = $jId",'one_touch_');
            $app = $appointment_model->getNextAppointment($jId);
        } /* fi is_null $jId */
        
        if ($this->debug) $this->log("putCancelBooking: AppointmentRecord  = \n".var_export($app, true),'one_touch_');
        
        if ( count($app) == 0 ) {                                               /* Check if we have matching appointments */
            $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'F';
            $r['RETURN_MESSAGE']['ERR_CODE'] = 015;
            $r['RETURN_MESSAGE']['ERR_MSG'] = "No appointment exists for service order";
            if ($this->debug) $this->log($r,'one_touch_');
            $rObj = $this->arrayToObject($r);
            if ($this->debug) $this->log($rObj,'one_touch_');
            return($rObj);
        }
        
        $spId = $app['ServiceProviderID'];
        $this->ServiceProviderID = $spId;
        $aDate = $app['AppointmentDate'];
        $eId = $app['ServiceProviderEngineerID'];
        
        if(! is_null($app['SBAppointID'])) {  
              $spinfo=$diary_model->getAllServiceProviderInfo($spId);
        if($spinfo['EmailAppBooking']=='Yes'){
            
            
            $ToEmail   = $spinfo['ServiceManagerEmail'];
            $CCEmails  = $spinfo['AdminSupervisorEmail'];
         
         
            // $ToEmail="a.polnikovs@pccsuk.com";
            $BCCEmails = "a.polnikovs@pccsuk.com;r.perry@pccsuk.com;j.berry@pccsuk.com;n.wrighting@pccsuk.com;".$spinfo['DiaryAdministratorEmail'];
           
            $emailModel = $this->loadModel('Email');//sending email if appointment created
            $emailModel->sendAppointmentEmail($app['AppointmentID'], 'Cancelled', $ToEmail, $CCEmails, $BCCEmails);/* Is there a Servicebase Appointment ID set */
            
           // $ToEmail=$spinfo['DiaryAdministratorEmail'];
          //  $emailModel->sendAppointmentEmail($app['AppointmentID'], 'Cancelled', $ToEmail, $CCEmails, false);/* Is there a Servicebase Appointment ID set */
            
        }
            
            $api_appointments_model->DeleteAppointment($app['AppointmentID']);  /* Yes - so make SB API call to delete */
        }
        
        $appointment_model->delete( array('AppointmentID' => $app['AppointmentID']) );    /* Delete the appointment */
        
        /* Run viamente if service provider uses it */
        if ($service_providers_model->getServiceCentreDiaryType($spId) == "FullViamente") {
            $viamente_model = $this->loadModel('APIViamente');
            $ret =  $viamente_model->OptimiseRoute($spId, $aDate, $eId);

            if(isset($ret->unreachedWaypointNames)&&sizeof($ret->unreachedWaypointNumbers)>0){
                $diary_model->setUnreached($ret->unreachedWaypointNames);
            }

            if(isset($ret->routes)){
                $diary_model->updateEngineerWorkload($ret->routes,$aDate,$spId);
            }
        }
        
        $r['RETURN_MESSAGE']['RETURN_TYPE'] = 'S';
        $r['RETURN_MESSAGE']['ERR_CODE'] = 0000;
   
       // }
        if ($this->debug) $this->log($r,'one_touch_');
        $rObj = $this->arrayToObject($r);
        if ($this->debug) $this->log($rObj,'one_touch_');
        $one_touch_audit_model->create($audit);
        return($rObj);
        
    }
    
    /**
     * testPutAPPAction
     * 
     * Test PutAppointmentDetails
     * 
     * @param array $args
     * 
     * @return 
     * 
     * @author Andris Polnikovs <a.polnikovs@pccsuk.com> 
     **************************************************************************/
    public function testPutAPPAction($args){
        $apiA=$this->loadModel('APIAppointments');
        print_r($args);
        
        $apiA->PutAppointmentDetails($args[0]);
        
    }
    
    /**
     * testJobIsNowSkylineNetAction
     * 
     * Test JobIsNowSkylineNet
     * 
     * @param array $args
     * 
     * @return 
     * 
     * @author Andris Polnikovs <a.polnikovs@pccsuk.com> 
     **************************************************************************/
    public function testJobIsNowSkylineNetAction($args){
        $apiA=$this->loadModel('NonSkylineJob');
        print_r($args);
        
        $apiA->JobIsNowSkylineNet($args[0], $args[1], $args[2]);
    }
    /* End Andris Test Procedures */
    
     private function arrayToObject($array) {
        if(!is_array($array)) {
            return $array;
        }

        $object = new stdClass();
        if (is_array($array) && count($array) > 0) {
          foreach ($array as $name=>$value) {
             $name = trim($name);
             if (!empty($name)) {
                $object->$name = $this->arrayToObject($value);
             }
          }
          return $object;
        }
        else {
          return FALSE;
        }
    }
    
}

?>
